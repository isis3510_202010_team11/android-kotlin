package com.example;

import android.app.Application;
import android.content.Context;

import com.google.firebase.database.FirebaseDatabase;

public class TuParqueApplication extends Application {

    private static Context context;

    @Override
    public void onCreate(){
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        TuParqueApplication.context = getApplicationContext();
    }

    public static Context getContext(){
        return TuParqueApplication.context;
    }
}
