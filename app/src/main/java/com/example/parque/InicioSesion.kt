package com.example.parque

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.parque.room.viewModel.LogInfoViewModel
import com.google.firebase.auth.FirebaseAuth
import java.io.*

private const val TAG = "PantallaInicial"
const val TPS = "TPS"
class InicioSesion : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var logInfoViewModel: LogInfoViewModel
    var cacheFile:File? = null
    private lateinit var cm:ConnectivityManager

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pantalla_inicial)
        supportActionBar?.hide()
        auth = FirebaseAuth.getInstance()
        logInfoViewModel = ViewModelProvider(this).get(LogInfoViewModel::class.java)
        cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    override fun onStart() {
        val sh = getSharedPreferences(TPS,Context.MODE_PRIVATE)
        val correo = sh.getString("correo", "")
        val input = findViewById<TextView>(R.id.inCorreoInicioSesion)
        input.text = correo
        super.onStart()
    }

    fun registrarUsuario(view: View) {
        val intent = Intent(this, RegistroUsuario::class.java)
        startActivity(intent)
        //finish()
    }

    fun iniciarSesion(view: View) {
        //Se valida si se cuenta con conexión o se está conectado a internet

        val txtNombre: TextView = findViewById(R.id.inCorreoInicioSesion)
        val strCorreo = txtNombre.text.toString()
        val txtContrasenia: TextView = findViewById(R.id.inContraseniaInicioSesion)
        val strContrasenia = txtContrasenia.text.toString()

        //Validacion de que haya texto en ambos campos
        if (strCorreo != "") {
            if (strContrasenia != "") {
                val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
                if (isConnected) {
                    auth.signInWithEmailAndPassword(strCorreo, strContrasenia)
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                val sharedPreferences = getSharedPreferences(TPS, Context.MODE_PRIVATE)
                                val editor = sharedPreferences.edit()
                                editor.putString("correo", strCorreo)
                                editor.apply()
                                Log.i(TAG, "Se guardó la información de inicio de sesión")
                                val intent = Intent(this, Mapa::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(intent)
                            } else {
                                val text = "Correo electrónico o contraseña incorrectos"
                                val duration = Toast.LENGTH_LONG
                                val toast = Toast.makeText(this, text, duration)
                                toast.show()
                            }
                        }
                }else{
                    val text = "Hubo un problema al intentar ingresar. Revisa tu conexión e intenta de nuevo"
                    val duration = Toast.LENGTH_LONG
                    val toast = Toast.makeText(this,text,duration)
                    toast.show()
                }

            }else {
                val texto = "Ingrese la contraseña"
                val duracion = Toast.LENGTH_SHORT
                val toast = Toast.makeText(this, texto, duracion)
                toast.show()
            }
        } else {
            val texto = "Ingrese un nombre de usuario"
            val duracion = Toast.LENGTH_SHORT
            val toast = Toast.makeText(this, texto, duracion)
            toast.show()
        }
    }
}
