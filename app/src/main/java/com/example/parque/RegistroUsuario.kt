package com.example.parque

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.parque.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_registro_usuario.*
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader

private const val TAG = "RegistroUsuario"
class RegistroUsuario : AppCompatActivity() {

    private lateinit var auth:FirebaseAuth
    var cacheFile:File? = null
    private lateinit var cm: ConnectivityManager

    val patronNombre:Regex = "[a-zA-Z\\u00C0-\\u017F\\s]+".toRegex()
    val patronCorreo:Regex = ("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*" +
            "|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]" +
            "|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9]" +
            "(?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
            "|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]" +
            "|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*" +
            "[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]" +
            "|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])").toRegex()
    private val patronFecha: Regex = "([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}\$".toRegex()

    private val patronGender: Regex = "(F|M|Otro)".toRegex()
    //8 caracteres
    //1 número
    //una mayúscula
    //una minúscula
    //un caracter especial
    val patronContrasenia:Regex = "[a-z|0-9]+".toRegex()


    override fun onCreate(savedInstanceState: Bundle?)
    {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_usuario)
        if(savedInstanceState!=null)
        {
            with(savedInstanceState){
                inNombreRegistro.setText(getString(STATE_NOMBRE))
                inFechaNacimiento.setText(getString(STATE_BIRTHDAY))
                inGenero.setText(getString(STATE_GENDER))
                inCorreoRegistro.setText(getString(STATE_CORREO))
            }
        }
        supportActionBar?.hide()
        auth = FirebaseAuth.getInstance()
        cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    override fun onResume()
    {
        if(cacheFile != null)
        {
            try {
                val fr = FileReader(cacheFile)
                val br = BufferedReader(fr)
                var line = br.readLine()
                var termino = 0
                while (line != null && termino < 4)
                {
                    if (line.contains("nombre:"))
                    {
                        val value = line.split(":")[1]
                        val txt = findViewById<TextView>(R.id.inNombreRegistro)
                        txt.text = value
                        termino++
                    }
                    else if(line.contains("birthday:"))
                    {
                        val value = line.split(":")[1]
                        val txt = findViewById<TextView>(R.id.inFechaNacimiento)
                        txt.text = value
                        termino++
                    }
                    else if(line.contains("gender:"))
                    {
                        val value = line.split("gender:")[1]
                        val txt = findViewById<TextView>(R.id.inGenero)
                        txt.text = value
                        termino ++
                    }
                    else if(line.contains("correo:"))
                    {
                        val value = line.split(":")[1]
                        val txt = findViewById<TextView>(R.id.inCorreoRegistro)
                        txt.text = value
                        termino++
                    }
                    line = br.readLine()
                }
                cacheFile!!.delete()
                br.close()
                fr.close()
            }
            catch (cause: Throwable)
            {
                Log.e(TAG, "No fue posible recuperar los datos de registro desde cache")
                cause.printStackTrace()
            }
        }
        super.onResume()
    }

    fun validarRegistro(view:View)
    {
        //Extraer informacion del formulario
        val txtNombre = findViewById<TextView>(R.id.inNombreRegistro)
        val strNombre = txtNombre.text.toString()
        Log.i(TAG, "Nombre: $strNombre")

        val txtBirthday = findViewById<TextView>(R.id.inFechaNacimiento)
        val strBirthday = txtBirthday.text.toString()
        Log.i(TAG, "Birthday: $strBirthday")

        val txtGender = findViewById<TextView>(R.id.inGenero)
        val strGender = txtGender.text.toString()
        Log.i(TAG, "Gender: $strGender")

        val txtCorreo:TextView = findViewById(R.id.inCorreoRegistro)
        val strCorreo:String = txtCorreo.text.toString()
        Log.i(TAG, "Correo: $strCorreo")

        val txtContrasenia1:TextView = findViewById(R.id.inContrasenia1)
        val strContrasenia1:String = txtContrasenia1.text.toString()

        val txtContrasenia2:TextView = findViewById(R.id.inContrasenia2)
        val strContrasenia2:String = txtContrasenia2.text.toString()


        //Verificacion de caracteres especiales en el nombre
        var errorNombre = false
        if(!strNombre.matches(patronNombre))
        {
            errorNombre = true
            Log.e(TAG, "El nombre contiene caracteres especiales")
        }
        //Verificación de caracteres especiales en el correo
        var errorCorreo = false
        if(!strCorreo.matches(patronCorreo))
        {
            errorCorreo = true
            Log.e(TAG, "El correo contiene caracteres no permitidos")
        }
        var errorBirthday = false
        if(!strBirthday.matches(patronFecha))
        {
            errorBirthday = true
            Log.e(TAG, "La fecha no sigue el formato necesitado")
        }
        var errorGender = false
        if(!strGender.matches(patronGender))
        {
            errorGender = true
            Log.e(TAG, "El genero no corresponde")
        }
        //Verificación de caracteres especiales en la constraseña
        var errorContrasenia1:Boolean = false
        if(!strContrasenia1.matches(patronContrasenia))
        {
            errorContrasenia1 = true
            Log.e(TAG, "La contraseña contiene caracteres especiales")
        }
        var errorContrasenia2:Boolean = false
        if(!strContrasenia1.equals(strContrasenia2))
        {
            errorContrasenia2 = true
            Log.e(TAG, "Las contraseñas no coinciden")
        }
        var textError = ""
        if(!errorNombre)
        {
            if(!errorCorreo) {
                if(!errorBirthday) {
                    if(!errorGender) {
                        if (!errorContrasenia1 && !errorContrasenia2) {
                            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                            val isConnected: Boolean =
                                activeNetwork?.isConnectedOrConnecting == true
                            if (isConnected) {
                                createAccount(strBirthday, strGender, strCorreo, strNombre, strContrasenia1)
                            } else {
                                textError =
                                    "Error en la conexión. Revisa tu conexión y reenvía el formulario"
                            }
                        } else {
                            textError =
                                "Revisa las contraseñas. Sólo pueden contener letras y números"
                        }
                    }else{
                        textError = "Revisa el campo de género"
                    }
                }else{
                    textError = "Revisa la fecha de nacimiento"
                }
            }else{
                textError = "Introduce una dirección de correo válida"
            }
        }else{
            textError = "El nombre solo puede contener letras"
        }
        if(textError.isNotEmpty()){
            val alertDialog = AlertDialog.Builder(this)
                .setTitle("ERROR")
                .setMessage(textError)
                .setIcon(R.drawable.ic_cancel_black_24dp)
                .setPositiveButton("OK",
                    object: DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            dialog!!.dismiss()
                        }
                    })
            alertDialog.show()
        }
    }

    private fun createAccount(birthday: String, gender: String,  mail:String, name:String, contrasenia:String)
    {
        Log.i(TAG, "Todos los parámetros funcionaron")
        auth.createUserWithEmailAndPassword(mail, contrasenia)
            .addOnCompleteListener(this){task ->
                if(task.isSuccessful)
                {
                    var authUser = auth.currentUser
                    var user = User(birthday, gender, mail, name)
                    if (authUser != null)
                        addUserToDb(authUser.uid, user)
                    Log.d(TAG, "createUserWithEmail:success")
                    val sharedPreferences = getSharedPreferences(TPS, Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putString("correo", mail)
                    editor.apply()
                    Log.i(TAG, "Se guardó la información de inicio de sesión")
                    val intent = Intent(this, Mapa::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                }
                else
                {
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                }
            }
    }

    override fun onSaveInstanceState(outState: Bundle)
    {
        val txtNombre = findViewById<TextView>(R.id.inNombreRegistro)
        val txtBirthday = findViewById<TextView>(R.id.inFechaNacimiento)
        val txtGender = findViewById<TextView>(R.id.inGenero)
        val txtCorreo = findViewById<TextView>(R.id.inCorreoRegistro)
        outState?.run {
            if(txtNombre != null)
                putString(STATE_NOMBRE, txtNombre.text.toString())
            if(txtBirthday != null)
                putString(STATE_BIRTHDAY, txtBirthday.text.toString())
            if(txtGender != null)
                putString(STATE_GENDER, txtGender.text.toString())
            if(txtCorreo != null)
                putString(STATE_CORREO, txtCorreo.text.toString())
        }
        super.onSaveInstanceState(outState)
    }

    companion object
    {
        const val STATE_NOMBRE      = "nombreRegistro"
        const val STATE_BIRTHDAY    = "birthdayRegistro"
        const val STATE_GENDER      = "genderRegistro"
        const val STATE_CORREO      = "correoRegistro"
    }


    private fun addUserToDb(uid:String, user:User)
    {
        Log.i(TAG, "Registrando usuario en bd.")
        var ref = FirebaseDatabase.getInstance().getReference("users")
        ref.child(uid).setValue(user)
        Log.i(TAG, "Fin de registro de usuario.")
    }

    override fun onPause()
    {
        val txtCorreo = findViewById<TextView>(R.id.inCorreoRegistro)
        val txtBirthday = findViewById<TextView>(R.id.inFechaNacimiento)
        val txtGender = findViewById<TextView>(R.id.inGenero)
        val txtNombre = findViewById<TextView>(R.id.inNombreRegistro)
        val correo = txtCorreo.text
        val birthday = txtBirthday.text
        val gender = txtGender.text
        val nombre = txtNombre.text

        try
        {
            val filename = "regInfoCache"
            val file = File.createTempFile(filename, null, applicationContext.cacheDir)
            val contents = "nombre:$nombre\nbirthday:$birthday\ngender:$gender\ncorreo:$correo"
            cacheFile = file
            val fos = FileOutputStream(file)
            fos.write(contents.toByteArray())
        }
        catch(cause:Throwable)
        {
            Log.e(TAG, "No fue posible guardar la información del textField durante la PAUSA")
            cause.printStackTrace()
        }
        txtNombre.text = ""
        txtBirthday.text = ""
        txtGender.text = ""
        txtCorreo.text = ""
        super.onPause()
    }

    override fun onDestroy()
    {
        cacheFile?.delete()
        super.onDestroy()
    }
}
