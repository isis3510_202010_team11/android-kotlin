package com.example.parque.fragments.activities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.parque.R
import com.example.parque.models.Court

class RecyclerAdapter (val context: Context, private val itemOnClickListener: OnItemClickListener): RecyclerView.Adapter<RecyclerAdapter.ViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private var activities: MutableList<Court> = mutableListOf<Court>()

    interface OnItemClickListener{
        fun onItemClicked(court: Court)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val name: TextView = itemView.findViewById(R.id.txtActivity)
        val cardView: CardView = itemView.findViewById(R.id.card)
        val image: ImageView = itemView.findViewById(R.id.imgActivity)

        fun bind(court: Court, clickListener: OnItemClickListener)
        {
            name.text = court.name
            Glide.with(context).load(court.imageURL).into(image)
            cardView.setOnClickListener{clickListener.onItemClicked(court)}
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflater.inflate(R.layout.card_view_activities, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val current = activities[position]
        holder.bind(current, itemOnClickListener)
    }

    internal fun setActivities(activities: List<Court>)
    {
        this.activities = activities as MutableList<Court>
        notifyDataSetChanged()
    }

    internal fun addActivity(activity: Court)
    {
        this.activities.add(activity)
        notifyDataSetChanged()
    }

    override fun getItemCount() = activities.size
}