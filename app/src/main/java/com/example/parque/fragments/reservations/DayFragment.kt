package com.example.parque.fragments.reservations

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.parque.ParqueReservas
import com.example.parque.R
import com.example.parque.models.Reserve
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_day.*
import kotlinx.android.synthetic.main.hour_day_view.view.*


private const val TAG ="Day fragment"

private const val PARAMS = "DAY_FRAGMENT PARAMS"

class DayFragment : Fragment()
{
    /**
     * Firebase conections.
     */
    private lateinit var db: FirebaseDatabase
    private lateinit var ref: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private lateinit var update: ValueEventListener

    /**
     * Date management.
     */
    private lateinit var day : String
    private lateinit var startHour: String
    private lateinit var endHour: String

    /**
     * List of possible hours.
     */
    private var hoursList : ArrayList<LinearLayout> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        // Now we update the buttons depending on the actual reservations.
        db = FirebaseDatabase.getInstance()
        ref = db.getReference("reserves")
        auth = FirebaseAuth.getInstance()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        var actHour = startHour.toInt() / 100
        var numRows = (endHour.toInt() - startHour.toInt()) / 100
        var layoutInflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        var i = 0
        while (i < numRows)
        {
//            Log.i(TAG, "Agregando fila. $i")
            var row : LinearLayout = layoutInflater.inflate(R.layout.hour_day_view, hours, false) as LinearLayout
            row.txtHour.text = "$actHour:00"
            row.bttHour.text = "Disponible"
            row.bttHour.setOnClickListener {
                val bttHour = row.txtHour.text
                (activity as ParqueReservas).attemptReservation("Comentario", "$day $bttHour:00", auth.currentUser?.email.toString())
            }
            hours.addView(row)
            hoursList.add(row)
            actHour++
            i++
        }

        update = object : ValueEventListener
        {
            override fun onDataChange(ds: DataSnapshot)
            {
                var index: ArrayList<Int> = ArrayList()
                for (snapshot in ds.children)
                {
                    var reserve: Reserve? = snapshot.getValue(Reserve::class.java)
                    if (reserve != null)
                        if (activity != null)
                            if (reserve.courtId == (activity as ParqueReservas).courtId)
                                if(reserve.start?.split(" ")?.get(0) == day)
                                {
                                    val resHour = reserve?.start?.split(" ")?.get(1)?.split(":")?.get(0)?.toInt()
                                    if (resHour != null)
                                        index.add(kotlin.math.abs((startHour.toInt() / 100) - resHour))
                                }
                }
                if(activity != null)
                {
                    var i = 0
                    while (i < numRows)
                    {
                        var btt = hoursList[i].bttHour
                        if (i in index)
                        {
                            btt.text = "No disponible"
                            btt.background = resources.getDrawable(R.drawable.rounded_buton_orange_border)
                        }
                        else
                        {
                            btt.text = "Disponible"
                            btt.background = resources.getDrawable(R.drawable.rounded_buton_blue_border)
                        }
                        i++
                    }
                }

            }
            override fun onCancelled(dbError: DatabaseError)
            {
                Log.i(TAG, "Update reservations canceled.")
            }
        }
        // Now we update it acording to the db.
        ref.orderByChild("start").startAt(day).addValueEventListener(update)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_day, container, false)
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        var params = arguments?.getString(PARAMS).toString()
        // The params for this fragment must come in this format (respecting the database standard)
        // dd/mm/yyyy hh:mm%hh:mm
        day = params.split(" ")[0]
        startHour = params.split(" ")[1].split("%")[0].replace(":", "")
        endHour = params.split(" ")[1].split("%")[1].replace(":", "")
    }

    override fun onDetach()
    {
        super.onDetach()
        this.day = null.toString()
        this.startHour = null.toString()
        this.endHour = null.toString()
        if(update!= null)
            this.ref.removeEventListener(update)
        (activity as ParqueReservas).onReturn()
    }

    companion object
    {
        fun newInstance(day: String, startHour: String, endHour: String) : DayFragment
        {
//            Log.i(TAG, "New instance.")
            val fragment = DayFragment()
            val arguments = Bundle()
            arguments.putString(PARAMS, "$day $startHour%$endHour")
            fragment.arguments = arguments
            return fragment
        }
    }
}
