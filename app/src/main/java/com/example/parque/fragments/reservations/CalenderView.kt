package com.example.parque.fragments.reservations

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.cardview.widget.CardView
import com.example.parque.R
import kotlinx.android.synthetic.main.day_date_view.view.*
import kotlinx.android.synthetic.main.day_header_view.view.*
import kotlinx.android.synthetic.main.event_line.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.abs


/**
 * Created by chintak on 28/9/17.
 * Updated and adapted by linkhl 04/04/2020
 */
class CalenderView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {

    private var MAX_EVENT = 3

    /**
     * Default event listener null.
     */
    private var eventClickListener: CalenderEventClickListener? = null

    /**
     * Default day listener.
     */
    private var dayClickListener: CalenderDayClickListener? = object :
        CalenderDayClickListener {
        override fun onDayClick(dayStr: String, monthStr: String, yearStr: String) {
            Toast.makeText(context, "$dayStr/$monthStr/$yearStr", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val monthYearFormat = "MMM yyyy"
        const val dateFormat = "dd/MM/yyyy"
        const val dateTimeFormat = "dd/MM/yyyy H:mm:ss"
        const val EXTRA_MARGIN = 15
        const val POST_TIME: Long = 100
    }

    private var eventList: ArrayList<EventItem> = ArrayList()
    private var calender: Calendar = Calendar.getInstance()
    private var layoutInflater =
        context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    private var dayViewHeader: View = layoutInflater.inflate(R.layout.day_header_view, this, false)
    private var dayViewRow1: FrameLayout =
        layoutInflater.inflate(R.layout.day_date_view, this, false) as FrameLayout
    private var dayViewRow2: FrameLayout =
        layoutInflater.inflate(R.layout.day_date_view, this, false) as FrameLayout
    private var dayViewRow3: FrameLayout =
        layoutInflater.inflate(R.layout.day_date_view, this, false) as FrameLayout
    private var dayViewRow4: FrameLayout =
        layoutInflater.inflate(R.layout.day_date_view, this, false) as FrameLayout
    private var dayViewRow5: FrameLayout =
        layoutInflater.inflate(R.layout.day_date_view, this, false) as FrameLayout
    private var dayViewRow6: FrameLayout =
        layoutInflater.inflate(R.layout.day_date_view, this, false) as FrameLayout

    init {
        orientation = VERTICAL
        addView(dayViewHeader)
        addView(dayViewRow1)
        addView(dayViewRow2)
        addView(dayViewRow3)
        addView(dayViewRow4)
        addView(dayViewRow5)
        addView(dayViewRow6)

        txt_monthTitle?.text =
            SimpleDateFormat(monthYearFormat, Locale.getDefault()).format(calender.time)
        btn_next?.setOnClickListener { nextMonth() }
        btn_previous?.setOnClickListener { previousMonth() }

        setOnTouchListener(object : OnCalendarSwipeListener(context) {
            override fun onSwipeLeft() {
                nextMonth()
            }

            override fun onSwipeRight() {
                previousMonth()
            }
        })
        updateEventView(true)
    }

    fun addEvent(eventItem: EventItem, clearOldData: Boolean = false) {

        if (clearOldData) eventList.clear()

        dayViewRow1.layout_tripEvents.removeAllViews()
        dayViewRow2.layout_tripEvents.removeAllViews()
        dayViewRow3.layout_tripEvents.removeAllViews()
        dayViewRow4.layout_tripEvents.removeAllViews()
        dayViewRow5.layout_tripEvents.removeAllViews()
        dayViewRow6.layout_tripEvents.removeAllViews()

        eventList.add(eventItem)
        val sortedList = eventList.sortedWith(compareBy { it.getStartDateToSort() })
        eventList.clear()
        eventList.addAll(sortedList)
        updateEventView()
    }

    fun addEventList(eventListItems: ArrayList<EventItem>, clearOldData: Boolean = false) {
        if (clearOldData)
            eventList.clear()

        dayViewRow1.layout_tripEvents.removeAllViews()
        dayViewRow2.layout_tripEvents.removeAllViews()
        dayViewRow3.layout_tripEvents.removeAllViews()
        dayViewRow4.layout_tripEvents.removeAllViews()
        dayViewRow5.layout_tripEvents.removeAllViews()
        dayViewRow6.layout_tripEvents.removeAllViews()

        eventList.addAll(eventListItems)
        val sortedList = eventListItems.sortedWith(compareBy { it.getStartDateToSort() })
        eventList.clear()
        eventList.addAll(sortedList)
        updateEventView()
    }

    private fun updateEventView(firstTime: Boolean = false)
    {
        var selectedCalender: Calendar = Calendar.getInstance()
        val actDay: Calendar = Calendar.getInstance()
        selectedCalender.set(calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DATE))
        selectedCalender.set(Calendar.DATE, 1)
        selectedCalender.set(Calendar.HOUR, 0)
        selectedCalender.set(Calendar.MINUTE, 0)
        selectedCalender.set(Calendar.SECOND, 0)

        calender.set(Calendar.DATE, 1)

        var totalDaysToShow: Int = calender.getActualMaximum(Calendar.DAY_OF_MONTH)
        val toFillInPreviousMonthDays = 1 - calender.get(Calendar.DAY_OF_WEEK)

        calender.set(Calendar.DATE, totalDaysToShow)

        val toFillInNextMonthDays = 7 - calender.get(Calendar.DAY_OF_WEEK)

        totalDaysToShow += abs(toFillInPreviousMonthDays) + toFillInNextMonthDays

        calender.set(Calendar.DATE, 1)

        if (toFillInPreviousMonthDays != 0)
            selectedCalender.add(Calendar.DAY_OF_YEAR, toFillInPreviousMonthDays)

        // Here The calendar defines the # of rows to show.
        when (totalDaysToShow.div(7))
        {
            4 -> {
                dayViewRow5.visibility = View.GONE
                dayViewRow6.visibility = View.GONE
            }
            5 -> {
                dayViewRow5.visibility = View.VISIBLE
                dayViewRow6.visibility = View.GONE
            }
            else -> {
                dayViewRow5.visibility = View.VISIBLE
                dayViewRow6.visibility = View.VISIBLE
            }
        }

        val dateFormatter = SimpleDateFormat(dateFormat, Locale.getDefault())

        // This refresh the calendar.
        dayViewRow1.layout_tripEvents.removeAllViews()
        dayViewRow2.layout_tripEvents.removeAllViews()
        dayViewRow3.layout_tripEvents.removeAllViews()
        dayViewRow4.layout_tripEvents.removeAllViews()
        dayViewRow5.layout_tripEvents.removeAllViews()
        dayViewRow6.layout_tripEvents.removeAllViews()

        for (i in 1..totalDaysToShow step 7) {
            when (i) {
                in 1..7 -> {
                    val layoutTripEvents = dayViewRow1.layout_tripEvents

                    val minDate = selectedCalender.time

                    val day1 = dayViewRow1.day1
                    day1.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day1.setTextColor(Color.GRAY)
                        day1.setOnClickListener(null)
                    }
                    else
                    {
                        day1.setTextColor(Color.BLACK)
                        day1.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day1.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)
                    var widthOfText = day1.measuredWidth


                    val day2 = dayViewRow1.day2
                    day2.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day2.setTextColor(Color.GRAY)
                        day2.setOnClickListener(null)
                    }
                    else
                    {
                        day2.setTextColor(Color.BLACK)
                        day2.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day2.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day3 = dayViewRow1.day3
                    day3.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day3.setTextColor(Color.GRAY)
                        day3.setOnClickListener(null)
                    }
                    else
                    {
                        day3.setTextColor(Color.BLACK)
                        day3.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day3.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day4 = dayViewRow1.day4
                    day4.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day4.setTextColor(Color.GRAY)
                        day4.setOnClickListener(null)
                    }
                    else
                    {
                        day4.setTextColor(Color.BLACK)
                        day4.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day4.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day5 = dayViewRow1.day5
                    day5.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day5.setTextColor(Color.GRAY)
                        day5.setOnClickListener(null)
                    }
                    else
                    {
                        day5.setTextColor(Color.BLACK)
                        day5.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day5.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day6 = dayViewRow1.day6
                    day6.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day6.setTextColor(Color.GRAY)
                        day6.setOnClickListener(null)
                    }
                    else
                    {
                        day6.setTextColor(Color.BLACK)
                        day6.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day6.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val maxDate = selectedCalender.time

                    val day7 = dayViewRow1.day7
                    day7.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day7.setTextColor(Color.GRAY)
                        day7.setOnClickListener(null)
                    }
                    else
                    {
                        day7.setTextColor(Color.BLACK)
                        day7.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day7.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    if (!firstTime) {
                        day1.postDelayed(
                            {
                                widthOfText = day1.measuredWidth
                                var eventAddCount = 0
                                for (j in 0 until eventList.size) {
                                    val startDate = dateFormatter.parse(eventList[j].start)
                                    val endDate = dateFormatter.parse(eventList[j].end)

                                    if (eventAddCount == MAX_EVENT)
                                        break

                                    startDate.hours = 0; startDate.minutes = 0; startDate.seconds =
                                        0
                                    endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

                                    if (startDate > endDate)
                                        continue

                                    val eventTrip = layoutInflater.inflate(
                                        R.layout.event_line,
                                        null
                                    ) as CardView
                                    val eventTitle = eventTrip.txt_eventTitle
                                    eventTrip.setCardBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTitle.setBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTitle.text = eventList[j].title
                                    eventTitle.setOnClickListener {
                                        eventClickListener?.onEventClick(eventList[j])
                                    }

                                    if (isDateInBetween(startDate, minDate, maxDate)) {
                                        val daysBetween = if (isSameDay(minDate, startDate)) 0 else getDaysBetween(minDate, startDate) + 1

                                        val startMarginDays = (widthOfText * daysBetween) + EXTRA_MARGIN
                                        var endMarginDays = 0

                                        if (isDateInBetween(endDate, minDate, maxDate))
                                            endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)

                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(endDate, minDate, maxDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(minDate, startDate, maxDate) && isDateInBetween(maxDate, startDate, endDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = 0

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                }
                            },
                            POST_TIME
                        )
                    }

                }
                in 8..14 -> {
                    val layoutTripEvents = dayViewRow2.layout_tripEvents

                    var widthOfText = 0

                    val minDate = selectedCalender.time

                    val day1 = dayViewRow2.day1
                    day1.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day1.setTextColor(Color.GRAY)
                        day1.setOnClickListener(null)
                    }
                    else
                    {
                        day1.setTextColor(Color.BLACK)
                        day1.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day1.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day2 = dayViewRow2.day2
                    day2.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day2.setTextColor(Color.GRAY)
                        day2.setOnClickListener(null)
                    }
                    else
                    {
                        day2.setTextColor(Color.BLACK)
                        day2.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day2.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day3 = dayViewRow2.day3
                    day3.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day3.setTextColor(Color.GRAY)
                        day3.setOnClickListener(null)
                    }
                    else
                    {
                        day3.setTextColor(Color.BLACK)
                        day3.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day3.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day4 = dayViewRow2.day4
                    day4.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day4.setTextColor(Color.GRAY)
                        day4.setOnClickListener(null)
                    }
                    else
                    {
                        day4.setTextColor(Color.BLACK)
                        day4.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day4.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day5 = dayViewRow2.day5
                    day5.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day5.setTextColor(Color.GRAY)
                        day5.setOnClickListener(null)
                    }
                    else
                    {
                        day5.setTextColor(Color.BLACK)
                        day5.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day5.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day6 = dayViewRow2.day6
                    day6.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day6.setTextColor(Color.GRAY)
                        day6.setOnClickListener(null)
                    }
                    else
                    {
                        day6.setTextColor(Color.BLACK)
                        day6.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day6.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val maxDate = selectedCalender.time

                    val day7 = dayViewRow2.day7
                    day7.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day7.setTextColor(Color.GRAY)
                        day7.setOnClickListener(null)
                    }
                    else
                    {
                        day7.setTextColor(Color.BLACK)
                        day7.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day7.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    if (!firstTime) {
                        day1.postDelayed(
                            {
                                widthOfText = day1.measuredWidth
                                var eventAddCount = 0
                                for (j in 0 until eventList.size)
                                {
                                    val startDate = dateFormatter.parse(eventList[j].start)
                                    val endDate = dateFormatter.parse(eventList[j].end)

                                    if (eventAddCount == MAX_EVENT)
                                        break

                                    startDate.hours = 0; startDate.minutes = 0; startDate.seconds = 0
                                    endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

                                    if (startDate > endDate)
                                        continue

                                    val eventTrip = layoutInflater.inflate(R.layout.event_line, null) as CardView
                                    eventTrip.setCardBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.setBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.text = eventList[j].title
                                    eventTrip.txt_eventTitle.setOnClickListener { eventClickListener?.onEventClick(eventList[j]) }

                                    if (isDateInBetween(startDate, minDate, maxDate))
                                    {
                                        val daysBetween = if (isSameDay(minDate, startDate)) 0 else getDaysBetween(minDate, startDate) + 1

                                        val startMarginDays = (widthOfText * daysBetween) + EXTRA_MARGIN
                                        var endMarginDays = 0

                                        if (isDateInBetween(endDate, minDate, maxDate))
                                            endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(endDate, minDate, maxDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(minDate, startDate, maxDate) && isDateInBetween(maxDate, startDate, endDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = 0

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                }
                            },
                            POST_TIME
                        )
                    }

                }
                in 15..21 -> {
                    val layoutTripEvents = dayViewRow3.layout_tripEvents

                    var widthOfText = 0
                    val minDate = selectedCalender.time
                    val day1 = dayViewRow3.day1
                    day1.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day1.setTextColor(Color.GRAY)
                        day1.setOnClickListener(null)
                    }
                    else
                    {
                        day1.setTextColor(Color.BLACK)
                        day1.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day1.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day2 = dayViewRow3.day2
                    day2.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day2.setTextColor(Color.GRAY)
                        day2.setOnClickListener(null)
                    }
                    else
                    {
                        day2.setTextColor(Color.BLACK)
                        day2.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day2.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day3 = dayViewRow3.day3
                    day3.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day3.setTextColor(Color.GRAY)
                        day3.setOnClickListener(null)
                    }
                    else
                    {
                        day3.setTextColor(Color.BLACK)
                        day3.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day3.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day4 = dayViewRow3.day4
                    day4.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day4.setTextColor(Color.GRAY)
                        day4.setOnClickListener(null)
                    }
                    else
                    {
                        day4.setTextColor(Color.BLACK)
                        day4.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day4.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day5 = dayViewRow3.day5
                    day5.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day5.setTextColor(Color.GRAY)
                        day5.setOnClickListener(null)
                    }
                    else
                    {
                        day5.setTextColor(Color.BLACK)
                        day5.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day5.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day6 = dayViewRow3.day6
                    day6.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day6.setTextColor(Color.GRAY)
                        day6.setOnClickListener(null)
                    }
                    else
                    {
                        day6.setTextColor(Color.BLACK)
                        day6.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day6.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val maxDate = selectedCalender.time

                    val day7 = dayViewRow3.day7
                    day7.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day7.setTextColor(Color.GRAY)
                        day7.setOnClickListener(null)
                    }
                    else
                    {
                        day7.setTextColor(Color.BLACK)
                        day7.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day7.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    if (!firstTime)
                    {
                        day1.postDelayed(
                            {
                                widthOfText = day1.measuredWidth
                                var eventAddCount = 0
                                for (j in 0 until eventList.size)
                                {
                                    val startDate = dateFormatter.parse(eventList[j].start)
                                    val endDate = dateFormatter.parse(eventList[j].end)

                                    if (eventAddCount == MAX_EVENT)
                                        break

                                    startDate.hours = 0; startDate.minutes = 0; startDate.seconds = 0
                                    endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

                                    if (startDate > endDate)
                                        continue

                                    val eventTrip = layoutInflater.inflate(R.layout.event_line, null) as CardView
                                    eventTrip.setCardBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.setBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.text = eventList[j].title
                                    eventTrip.txt_eventTitle.setOnClickListener { eventClickListener?.onEventClick(eventList[j]) }

                                    if (isDateInBetween(startDate, minDate, maxDate))
                                    {
                                        val daysBetween = if (isSameDay(minDate, startDate)) 0 else getDaysBetween(minDate, startDate) + 1

                                        val startMarginDays = (widthOfText * daysBetween) + EXTRA_MARGIN
                                        var endMarginDays = 0

                                        if (isDateInBetween(endDate, minDate, maxDate))
                                            endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)

                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(endDate, minDate, maxDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(minDate, startDate, maxDate) && isDateInBetween(maxDate, startDate, endDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = 0

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                }
                            },
                            POST_TIME
                        )
                    }

                }
                in 22..28 -> {
                    val layoutTripEvents = dayViewRow4.layout_tripEvents

                    var widthOfText = 0
                    val minDate = selectedCalender.time
                    val day1 = dayViewRow4.day1
                    day1.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day1.setTextColor(Color.GRAY)
                        day1.setOnClickListener(null)
                    }
                    else
                    {
                        day1.setTextColor(Color.BLACK)
                        day1.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day1.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day2 = dayViewRow4.day2
                    day2.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day2.setTextColor(Color.GRAY)
                        day2.setOnClickListener(null)
                    }
                    else
                    {
                        day2.setTextColor(Color.BLACK)
                        day2.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day2.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day3 = dayViewRow4.day3
                    day3.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day3.setTextColor(Color.GRAY)
                        day3.setOnClickListener(null)
                    }
                    else
                    {
                        day3.setTextColor(Color.BLACK)
                        day3.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day3.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day4 = dayViewRow4.day4
                    day4.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day4.setTextColor(Color.GRAY)
                        day4.setOnClickListener(null)
                    }
                    else
                    {
                        day4.setTextColor(Color.BLACK)
                        day4.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day4.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day5 = dayViewRow4.day5
                    day5.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day5.setTextColor(Color.GRAY)
                        day5.setOnClickListener(null)
                    }
                    else
                    {
                        day5.setTextColor(Color.BLACK)
                        day5.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day5.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day6 = dayViewRow4.day6
                    day6.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day6.setTextColor(Color.GRAY)
                        day6.setOnClickListener(null)
                    }
                    else
                    {
                        day6.setTextColor(Color.BLACK)
                        day6.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day6.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val maxDate = selectedCalender.time
                    val day7 = dayViewRow4.day7
                    day7.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day7.setTextColor(Color.GRAY)
                        day7.setOnClickListener(null)
                    }
                    else
                    {
                        day7.setTextColor(Color.BLACK)
                        day7.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day7.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    if (!firstTime) {
                        day1.postDelayed(
                            {
                                widthOfText = day1.measuredWidth
                                var eventAddCount = 0
                                for (j in 0 until eventList.size)
                                {
                                    val startDate = dateFormatter.parse(eventList.get(j).start)
                                    val endDate = dateFormatter.parse(eventList.get(j).end)

                                    if (eventAddCount == MAX_EVENT)
                                        break

                                    startDate.hours = 0; startDate.minutes = 0; startDate.seconds = 0
                                    endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

                                    if (startDate > endDate)
                                        continue

                                    val eventTrip = layoutInflater.inflate(R.layout.event_line, null) as CardView
                                    eventTrip.setCardBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.setBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.text = eventList[j].title
                                    eventTrip.txt_eventTitle.setOnClickListener { eventClickListener?.onEventClick(eventList[j]) }
                                    if (isDateInBetween(startDate, minDate, maxDate))
                                    {
                                        val daysBetween = if (isSameDay(minDate, startDate)) 0 else getDaysBetween(minDate, startDate) + 1

                                        val startMarginDays = (widthOfText * daysBetween) + EXTRA_MARGIN
                                        var endMarginDays = 0

                                        if (isDateInBetween(endDate, minDate, maxDate))
                                            endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)

                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(endDate, minDate, maxDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(minDate, startDate, maxDate) && isDateInBetween(maxDate, startDate, endDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = 0

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                }
                            },
                            POST_TIME
                        )
                    }
                }
                in 29..35 -> {
                    val layoutTripEvents = dayViewRow5.layout_tripEvents

                    var widthOfText = 0

                    val minDate = selectedCalender.time
                    val day1 = dayViewRow5.day1
                    day1.text = selectedCalender.get(Calendar.DATE).toString()

                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day1.setTextColor(Color.GRAY)
                        day1.setOnClickListener(null)
                    }
                    else
                    {
                        day1.setTextColor(Color.BLACK)
                        day1.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day1.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day2 = dayViewRow5.day2
                    day2.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day2.setTextColor(Color.GRAY)
                        day2.setOnClickListener(null)
                    }
                    else
                    {
                        day2.setTextColor(Color.BLACK)
                        day2.setOnClickListener { dayClickListener?.onDayClick(day2.text as String, "" + (selectedCalender.get(Calendar.MONTH) + 1), "" + selectedCalender.get(Calendar.YEAR)) }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day3 = dayViewRow5.day3
                    day3.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day3.setTextColor(Color.GRAY)
                        day3.setOnClickListener(null)
                    }
                    else
                    {
                        day3.setTextColor(Color.BLACK)
                        day3.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day3.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day4 = dayViewRow5.day4
                    day4.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day4.setTextColor(Color.GRAY)
                        day4.setOnClickListener(null)
                    }
                    else
                    {
                        day4.setTextColor(Color.BLACK)
                        day4.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day4.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day5 = dayViewRow5.day5
                    day5.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day5.setTextColor(Color.GRAY)
                        day5.setOnClickListener(null)
                    }
                    else
                    {
                        day5.setTextColor(Color.BLACK)
                        day5.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day5.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day6 = dayViewRow5.day6
                    day6.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day6.setTextColor(Color.GRAY)
                        day6.setOnClickListener(null)
                    }
                    else
                    {
                        day6.setTextColor(Color.BLACK)
                        day6.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day6.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val maxDate = selectedCalender.time
                    val day7 = dayViewRow5.day7
                    day7.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day7.setTextColor(Color.GRAY)
                        day7.setOnClickListener(null)
                    }
                    else
                    {
                        day7.setTextColor(Color.BLACK)
                        day7.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day7.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    if (!firstTime)
                    {
                        day1.postDelayed(
                            {
                                widthOfText = day1.measuredWidth
                                var eventAddCount = 0
                                for (j in 0 until eventList.size)
                                {
                                    val startDate = dateFormatter.parse(eventList[j].start)
                                    val endDate = dateFormatter.parse(eventList[j].end)

                                    if (eventAddCount == MAX_EVENT)
                                        break

                                    startDate.hours = 0; startDate.minutes = 0; startDate.seconds = 0
                                    endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

                                    if (startDate > endDate)
                                        continue

                                    val eventTrip = layoutInflater.inflate(R.layout.event_line, null) as CardView
                                    eventTrip.setCardBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.setBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.text = eventList[j].title
                                    eventTrip.txt_eventTitle.setOnClickListener {
                                        eventClickListener?.onEventClick(
                                            eventList[j]
                                        )
                                    }
                                    if (isDateInBetween(startDate, minDate, maxDate))
                                    {
                                        val daysBetween = if (isSameDay(minDate, startDate)) 0 else getDaysBetween(minDate, startDate) + 1

                                        val startMarginDays = (widthOfText * daysBetween) + EXTRA_MARGIN
                                        var endMarginDays = 0

                                        if (isDateInBetween(endDate, minDate, maxDate))
                                            endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)

                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(endDate, minDate, maxDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(minDate, startDate, maxDate) && isDateInBetween(maxDate, startDate, endDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = 0

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                }
                            },
                            POST_TIME
                        )
                    }
                }
                in 36..42 -> {

                    val layoutTripEvents = dayViewRow6.layout_tripEvents

                    var widthOfText = 0

                    val minDate = selectedCalender.time
                    val day1 = dayViewRow6.day1
                    day1.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day1.setTextColor(Color.GRAY)
                        day1.setOnClickListener(null)
                    }
                    else
                    {
                        day1.setTextColor(Color.BLACK)
                        day1.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day1.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day2 = dayViewRow6.day2
                    day2.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day2.setTextColor(Color.GRAY)
                        day2.setOnClickListener(null)
                    }
                    else
                    {
                        day2.setTextColor(Color.BLACK)
                        day2.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day2.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day3 = dayViewRow6.day3
                    day3.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day3.setTextColor(Color.GRAY)
                        day3.setOnClickListener(null)
                    }
                    else
                    {
                        day3.setTextColor(Color.BLACK)
                        day3.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day3.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    val day4 = dayViewRow6.day4
                    day4.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day4.setTextColor(Color.GRAY)
                        day4.setOnClickListener(null)
                    }
                    else
                    {
                        day4.setTextColor(Color.BLACK)
                        day4.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day4.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day5 = dayViewRow6.day5
                    day5.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day5.setTextColor(Color.GRAY)
                        day5.setOnClickListener(null)
                    }
                    else
                    {
                        day5.setTextColor(Color.BLACK)
                        day5.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day5.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val day6 = dayViewRow6.day6
                    day6.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day6.setTextColor(Color.GRAY)
                        day6.setOnClickListener(null)
                    }
                    else
                    {
                        day6.setTextColor(Color.BLACK)
                        day6.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day6.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)


                    val maxDate = selectedCalender.time
                    val day7 = dayViewRow6.day7
                    day7.text = selectedCalender.get(Calendar.DATE).toString()
                    if (selectedCalender.get(Calendar.DAY_OF_YEAR) < actDay.get(Calendar.DAY_OF_YEAR))
                    {
                        day7.setTextColor(Color.GRAY)
                        day7.setOnClickListener(null)
                    }
                    else
                    {
                        day7.setTextColor(Color.BLACK)
                        day7.setOnClickListener {
                            dayClickListener?.onDayClick(
                                day7.text as String,
                                "" + (selectedCalender.get(Calendar.MONTH)),
                                "" + selectedCalender.get(Calendar.YEAR)
                            )
                        }
                    }
                    selectedCalender = addSingleDay(selectedCalender)

                    if (!firstTime) {
                        day1.postDelayed(
                            {
                                widthOfText = day1.measuredWidth
                                var eventAddCount = 0
                                for (j in 0 until eventList.size) {
                                    val startDate = dateFormatter.parse(eventList[j].start)
                                    val endDate = dateFormatter.parse(eventList[j].end)

                                    if (eventAddCount == MAX_EVENT)
                                        break

                                    startDate.hours = 0; startDate.minutes = 0; startDate.seconds =
                                        0
                                    endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

                                    if (startDate > endDate)
                                        continue

                                    val eventTrip = layoutInflater.inflate(R.layout.event_line, null) as CardView
                                    eventTrip.setCardBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.setBackgroundColor(Color.parseColor(eventList[j].color))
                                    eventTrip.txt_eventTitle.text = eventList[j].title
                                    eventTrip.txt_eventTitle.setOnClickListener { eventClickListener?.onEventClick(eventList[j])
                                    }
                                    if (isDateInBetween(startDate, minDate, maxDate))
                                    {
                                        val daysBetween = if (isSameDay(minDate, startDate)) 0 else getDaysBetween(minDate, startDate) + 1

                                        val startMarginDays = (widthOfText * daysBetween) + EXTRA_MARGIN
                                        var endMarginDays = 0

                                        if (isDateInBetween(endDate, minDate, maxDate)) { endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText }

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)

                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(endDate, minDate, maxDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = (widthOfText * getDaysBetween(endDate, maxDate)) + EXTRA_MARGIN + widthOfText

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                    else if (isDateInBetween(minDate, startDate, maxDate) && isDateInBetween(maxDate, startDate, endDate))
                                    {
                                        val startMarginDays = 0
                                        val endMarginDays = 0

                                        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                                        params.setMargins(startMarginDays, 5, endMarginDays, 5)
                                        layoutTripEvents.addView(eventTrip, params)
                                        eventAddCount++
                                    }
                                }
                            },
                            POST_TIME
                        )
                    }
                }
            }
        }
    }

    private fun setMonthTextColor(color: Int)
    {
        txt_monthTitle.setTextColor(color)
    }

    private fun setWeekDayTitleTextColor(color: Int)
    {
        txt_sun.setTextColor(color)
        txt_mon.setTextColor(color)
        txt_tue.setTextColor(color)
        txt_wed.setTextColor(color)
        txt_thu.setTextColor(color)
        txt_fri.setTextColor(color)
        txt_sat.setTextColor(color)
    }

    fun setHeaderColor(color: Int)
    {
        setMonthTextColor(color)
        setWeekDayTitleTextColor(color)
    }

    private fun addSingleDay(calendar: Calendar): Calendar
    {
        val currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        val currentDayOfYear = calendar.get(Calendar.DAY_OF_YEAR)

        val copyCalender: Calendar = calendar.clone() as Calendar
        copyCalender.set(Calendar.DATE, 1)

        val maxDaysInMonth = copyCalender.getMaximum(Calendar.DAY_OF_MONTH)

        if (currentDayOfMonth == maxDaysInMonth)
            if (currentDayOfYear == calendar.getActualMaximum(Calendar.DAY_OF_YEAR))
            {
                calendar.add(Calendar.YEAR, 1)
                calendar.set(Calendar.MONTH, Calendar.JANUARY)
                calendar.set(Calendar.DAY_OF_YEAR, 1)
            }
            else
            {
                calendar.add(Calendar.MONTH, 1)
                calendar.set(Calendar.DATE, 1)
            }
        else
            calendar.add(Calendar.DAY_OF_YEAR, 1)

        return calendar
    }

    fun nextMonth()
    {
        calender.add(Calendar.MONTH, 1)
        txt_monthTitle?.text = SimpleDateFormat(monthYearFormat, Locale.getDefault()).format(calender.time)
        updateEventView()
    }

    fun previousMonth()
    {
        calender.add(Calendar.MONTH, -1)
        txt_monthTitle?.text =
            SimpleDateFormat(monthYearFormat, Locale.getDefault()).format(calender.time)
        updateEventView()
    }

    private fun isDateInBetween(date: Date, minDate: Date, maxDate: Date): Boolean {
        date.hours = 0; date.minutes = 0; date.seconds = 0
        minDate.hours = 0; minDate.minutes = 0; minDate.seconds = 0
        maxDate.hours = 0; maxDate.minutes = 0; maxDate.seconds = 0
        if (isSameDay(date, minDate) || isSameDay(date, maxDate))
            return true

        if (date in minDate..maxDate)
            return true

        return false
    }

    private fun isSameDay(date1: Date, date2: Date): Boolean
    {
        val calender1 = Calendar.getInstance()
        val calender2 = Calendar.getInstance()
        calender1.time = date1; calender2.time = date2
        return calender1.get(Calendar.YEAR) == calender2.get(Calendar.YEAR) && calender1.get(Calendar.DAY_OF_YEAR) == calender2.get(Calendar.DAY_OF_YEAR)
    }

    private fun getDaysBetween(startDate: Date, endDate: Date): Int {
        startDate.hours = 0; startDate.minutes = 0; startDate.seconds = 0
        endDate.hours = 0; endDate.minutes = 0; endDate.seconds = 0

        val diff = endDate.time - startDate.time

        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toInt()
    }

    fun setCalenderDayClickListener(calendarDayClickListener: CalenderDayClickListener)
    {
        this.dayClickListener = calendarDayClickListener
    }

    interface CalenderDayClickListener
    {
        fun onDayClick(dayStr: String, monthStr: String, yearStr: String)
    }

    fun setCalenderEventClickListener(eventClickListener: CalenderEventClickListener)
    {
        this.eventClickListener = eventClickListener
    }

    interface CalenderEventClickListener
    {
        fun onEventClick(eventItem: EventItem)
    }
}