package com.example.parque.fragments.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.parque.EXTRA_MSG
import com.example.parque.ParqueReservas
import com.example.parque.R
import com.example.parque.models.Court
import com.google.firebase.database.*


private const val TAG ="Sports Fragment"
private const val TYPE = "Deportivo"
private const val PARK_ID = "Park id param"

class SportsFragment : Fragment(), RecyclerAdapter.OnItemClickListener {

    /**
     * Firebase conections.
     */
    private lateinit var db: FirebaseDatabase
    private lateinit var ref: DatabaseReference

    /**
     * Views and data.
     */
    private lateinit var parkId: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_sports, container, false)
        val sportsRecycler = root.findViewById<RecyclerView>(R.id.sports_recycler)
        val sportsAdapter = RecyclerAdapter(root.context, this)
        db = FirebaseDatabase.getInstance()
        ref = db.getReference("courts")

        // Set sports recycler view.
        sportsRecycler.setHasFixedSize(true)
        sportsRecycler.adapter = sportsAdapter
        sportsRecycler.layoutManager = GridLayoutManager(context, 2)

        ref.orderByChild("parkId").equalTo(parkId).addListenerForSingleValueEvent(
            object : ValueEventListener
            {
                override fun onDataChange(ds: DataSnapshot)
                {
                    val courts = ds.children
                    courts.forEach { temp ->
                        // Loading the wanted courts.
                        var court = temp.getValue(Court::class.java)
                        if (court != null)
                        {
                            court.id = temp.key
                            if (court.type == TYPE)
                                sportsAdapter.addActivity(court)
                        }
                    }
                }
                override fun onCancelled(dbError: DatabaseError)
                {
                    Log.i(TAG, "Courts name canceled.")
                }
            }
        )
        return root
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        parkId = arguments?.getString(PARK_ID).toString()
    }

    companion object
    {
        fun newInstance(parkId:String) : SportsFragment
        {
            Log.i(TAG, "New instance")
            val fragment =
                SportsFragment()
            val arguments = Bundle()
            arguments.putString(PARK_ID, parkId)
            fragment.arguments = arguments
            return fragment
        }
    }

    // -----------------------------------------------------------------
    // Aux methods.
    // -----------------------------------------------------------------

    override fun onItemClicked(court: Court)
    {
        val intent = Intent(context, ParqueReservas::class.java)
        intent.apply {
            putExtra(EXTRA_MSG, court.id)
        }
        startActivity(intent)
    }
}