package com.example.parque

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.parque.fragments.activities.ActividadesAdapter
import com.example.parque.fragments.activities.NonSportsFragment
import com.example.parque.fragments.activities.SportsFragment
import com.example.parque.models.Park
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_parque_actividades.*

/**
 * TAG for the log.
 */
private const val TAG = "Park View"

/**
 * Constraint for the information to init the park.
 */
const val EXTRA_MSG = "com.example.parque.MESSAGE"


class ParqueActividades : AppCompatActivity(){

    /**
     * Firebase conections.
     */
    private lateinit var db: FirebaseDatabase
    private lateinit var ref: DatabaseReference

    /**
     * Park id.
     */
    private lateinit var parkId: String

    override fun onCreate(savedInstanceState: Bundle?)
    {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parque_actividades)

        // Load park information from firebase.
        parkId = intent.getStringExtra(EXTRA_MSG).toString()
        val sharedPref = this.getSharedPreferences("PREF_PARK",Context.MODE_PRIVATE)
        with (sharedPref.edit()) {
            putString("PARK_ID", parkId)
            commit()
        }


        if(isOnline())
        {
            Log.i(TAG, "ON LINE")
            db = FirebaseDatabase.getInstance()
            ref = db.getReference("parks")
            ref.child(parkId).addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(ds: DataSnapshot) {
                        var park = ds.getValue(Park::class.java)
                        if (park != null) {
                            txtNombreParque.text = park.name
                            Glide.with(applicationContext).load(park.imageURL).into(parkImage)
                        }
                    }

                    override fun onCancelled(dbError: DatabaseError) {
                        Log.i(TAG, "Park name canceled.")
                    }
                }
            )
        }
        else
            Log.i(TAG, "OFF LINE")
        val adapter = ActividadesAdapter(supportFragmentManager)

//        Log.i(TAG, "Creating Sports fragment.")
        val sports = SportsFragment.newInstance(parkId)
        adapter.addFragment(sports, "Deportivos")
//        Log.i(TAG, "Creating Non Sports fragment.")
        val nonSportsFragment = NonSportsFragment.newInstance(parkId)
        adapter.addFragment(nonSportsFragment, "No Deportivos")


        fragmentContainer.adapter = adapter
        tabParkActivities.setupWithViewPager(fragmentContainer, true)

        supportActionBar?.hide()
    }


    // -----------------------------------------------------------------
    // Connectivity methods.
    // -----------------------------------------------------------------

    private fun isOnline(): Boolean
    {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}