package com.example.parque

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.parque.models.Court
import com.example.parque.models.Reserve
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
private const val TAG = "ReserveQR"
class ReserveQR : AppCompatActivity() {

    private lateinit var db:FirebaseDatabase
    private lateinit var reserveId:String

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reserve_q_r)
        db = FirebaseDatabase.getInstance()
        reserveId = intent.getStringExtra(EXTRA_MSG).toString()
        supportActionBar?.hide()
    }

    override fun onStart() {
        super.onStart()
        val referenceReserva = db.getReference("reserves").child(reserveId)
        referenceReserva.addListenerForSingleValueEvent(
            object:ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(ds: DataSnapshot) {
                    val reserve = ds.getValue(Reserve::class.java)
                    val start = reserve!!.start
                    val courtId = reserve.courtId
                    val text = ds.key
                    val referenciaCourt = db.getReference("courts").child(courtId!!)
                    referenciaCourt.addListenerForSingleValueEvent(
                        object:ValueEventListener{
                            override fun onDataChange(ds: DataSnapshot) {
                                val court: Court? = ds.getValue(Court::class.java)
                                val courtName = court!!.name
                                val txtNombreCourt = findViewById<TextView>(R.id.txtCanchaQR)
                                val txtFecha = findViewById<TextView>(R.id.txtFechaQR)
                                txtNombreCourt.text = courtName
                                txtFecha.text = start
                                val qrEncoder:QRGEncoder = QRGEncoder(text,null,QRGContents.Type.TEXT, 500)
                                try{
                                    val bitmap = qrEncoder.encodeAsBitmap()
                                    val image = findViewById<ImageView>(R.id.qrReservaUsuario)
                                    image.setImageBitmap(bitmap)
                                    val layout = findViewById<ConstraintLayout>(R.id.qrLayout)
                                }catch(cause:Throwable){
                                    val intent = Intent(applicationContext, ReservasUsuario::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    cause.printStackTrace()
                                    Toast.makeText(applicationContext, "No fue posible generar el código QR", Toast.LENGTH_SHORT)
                                        .show()
                                    startActivity(intent)
                                    cause.printStackTrace()
                                }
                            }

                            override fun onCancelled(p0: DatabaseError) {
                                TODO("Not yet implemented")
                            }
                        }
                    )

                }

            }
        )
    }

    fun cancelarReserva(view:View){
        val alertDialog = AlertDialog.Builder(this)
            .setTitle("Cancelar Reserva")
            .setMessage("Confirmar cancelación de reserva")
            .setIcon(R.drawable.ic_cancel_black_24dp)
            .setPositiveButton("Cancelar Reserva",
            object:DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    val refereciaReserve = db.getReference("reserves").child("$reserveId").removeValue()
                    Log.i(TAG,"Se canceló la reserva $reserveId")
                    dialog!!.dismiss()
                    val toast = Toast.makeText(applicationContext, "Se canceló tu reserva", Toast.LENGTH_SHORT)
                    toast.show()
                    val intent = Intent(applicationContext, ReservasUsuario::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                }
            })
            .setNegativeButton("No Cancelar Reserva",
            object:DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                }
            })
        alertDialog.show()
    }


}
