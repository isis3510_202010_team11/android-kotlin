package com.example.parque

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.*
import android.net.ConnectivityManager.NetworkCallback
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.marginBottom
import com.example.parque.models.Park
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


private const val TAG = "Mapa"
class Mapa : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener{

    private lateinit var mMap: GoogleMap
    private lateinit var auth:FirebaseAuth
    private val baseDatos = Firebase.database
    private var snackbar: Snackbar? = null
    private var isConnected:Boolean = true
    private var monitoringConnectivity:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser?.reload()
        if(user == null){
            val intent = Intent(this, InicioSesion::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }else{
            setContentView(R.layout.activity_mapa)
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync(this)
            supportActionBar?.hide()
            val view = findViewById<CoordinatorLayout>(R.id.myCoordinatorLayout)
            val text = "No te encuentras conectado a internet"
            snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE)
            snackbar?.setBackgroundTint(Color.RED)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Sydney and move the camera
        val bogota = LatLng(4.7110, -74.0721)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bogota, 10F))
        mMap.setOnMarkerClickListener(this)
        leerDireccionesParques()
    }

    fun leerDireccionesParques()
    {
        val referencia = baseDatos.getReference("parks")
        referencia.addValueEventListener(
            object : ValueEventListener
            {
                override fun onDataChange(ds: DataSnapshot)
                {
                    for (snapshot in ds.children) {
                        var park: Park? = snapshot.getValue(Park::class.java)
                        val latitude = park?.latitude
                        val longitude = park?.longitude
                        val position = LatLng(latitude!!, longitude!!)
                        val name = park?.name
                        val address = park?.address
                        val key = snapshot.key
                        val city = park?.city
                        val imageURL = park?.imageURL
                        val marker = mMap.addMarker(MarkerOptions().position(position).title(name!!))
                        //Se agrega el id de cada parque al marker
                        marker.tag = key
                    }
                }
                override fun onCancelled(dbError: DatabaseError)
                {
                    Log.i(TAG, "Parks canceled.")
                }
            }
        )
    }

    private val connectivityCallback: NetworkCallback = object : NetworkCallback() {
        override fun onAvailable(network: Network?) {
            snackbar?.dismiss()
            isConnected = true
            Log.i(TAG, "INTERNET CONNECTED")
        }

        override fun onLost(network: Network?) {
            isConnected = false
            Log.i(TAG, "INTERNET LOST")
            snackbar?.show()
        }
    }


    override fun onResume() {
        super.onResume()
        checkConnectivity()
    }

    private fun checkConnectivity() {
        // here we are getting the connectivity service from connectivity manager
        val connectivityManager = getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager

        // Getting network Info
        // give Network Access Permission in Manifest
        val activeNetworkInfo = connectivityManager.activeNetworkInfo

        // isConnected is a boolean variable
        // here we check if network is connected or is getting connected
        isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        // SHOW ANY ACTION YOU WANT TO SHOW
        // WHEN WE ARE NOT CONNECTED TO INTERNET/NETWORK
        // if Network is not connected we will register a network callback to  monitor network
        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .build(), connectivityCallback
        )
        monitoringConnectivity = true
        if(!isConnected){
            snackbar!!.show()
        }
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        val intent = Intent(this, ParqueActividades::class.java)
        var parkId = marker?.tag.toString()
        intent.apply {
            putExtra(EXTRA_MSG, parkId)
        }
        startActivity(intent)
        return true
    }

    override fun onPause() {
        if (monitoringConnectivity) {
            val connectivityManager =
                getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            connectivityManager.unregisterNetworkCallback(connectivityCallback)
            monitoringConnectivity = false
        }
        super.onPause()
    }


    fun callAR(view:View){
        val intent = Intent(this, CameraActivity::class.java)
        startActivity(intent)
    }

    fun callMisReservas(view:View){
        val intent = Intent(this, ReservasUsuario::class.java)
        startActivity(intent)
    }

}
