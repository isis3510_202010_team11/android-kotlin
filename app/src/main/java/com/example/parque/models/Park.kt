package com.example.parque.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Park(
    var address : String? = "",
    var city : String? = "",
    var name : String? = "",
    var imageURL : String? = "",
    var latitude:Double? = 0.0,
    var longitude:Double? = 0.0
)