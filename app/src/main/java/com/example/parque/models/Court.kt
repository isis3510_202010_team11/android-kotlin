package com.example.parque.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Court (
    var id          : String? = "",
    var imageURL    : String? = "",
    var name        : String? = "",
    var parkId      : String? = "",
    var type        : String? = ""
)