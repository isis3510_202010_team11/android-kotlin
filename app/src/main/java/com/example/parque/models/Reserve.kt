package com.example.parque.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Reserve (
    var comment : String? = "",
    var courtId : String? = "",
    var start   : String? = "",
    var user    : String? = "",
    var state : String? = ""

)