package com.example.parque.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class User(
    var birthdate   : String? = "",
    var gender      : String?= "",
    var mail        : String? = "",
    var name        : String? = ""
)