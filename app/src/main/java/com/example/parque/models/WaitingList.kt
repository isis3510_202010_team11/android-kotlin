package com.example.parque.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class WaitingList (
    var courtId : String = "",
    var users   : List<User>
)