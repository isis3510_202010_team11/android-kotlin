package com.example.parque

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.parque.fragments.reservations.CalenderView
import com.example.parque.fragments.reservations.DayFragment
import com.example.parque.models.Court
import com.example.parque.models.Reserve
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_parque_reservas.*
import kotlinx.android.synthetic.main.custom_dialog_class.view.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


private const val TAG = "Reserves"
class ParqueReservas : AppCompatActivity()
{
    /**
     * Firebase connection.
     */
    private lateinit var db : FirebaseDatabase
    private lateinit var ref : DatabaseReference

    /**
     * Views and data.
     */
    private lateinit var calendario : CalenderView
    lateinit var courtId: String

    /**
     * Connectivity management.
     */
    private var isConnected: Boolean = true
    private var monitoringConnectivity: Boolean = false


    /**
     * Build view.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parque_reservas)
        supportActionBar?.hide()

        // Load court information from firebase.
        db = FirebaseDatabase.getInstance()
        courtId = intent.getStringExtra(EXTRA_MSG)
        ref = db.getReference("courts")
        ref.child(courtId).addListenerForSingleValueEvent(
            object : ValueEventListener
            {
                override fun onDataChange(ds: DataSnapshot)
                {
                    var court = ds.getValue(Court::class.java)
                    if(court != null)
                        updateUI(court)
                }
                override fun onCancelled(databaseError: DatabaseError)
                {
                    Log.d(TAG, "Canceled")
                }
            }
        )
        var act = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH"))
        act = "$act:00:00"
        ref = db.getReference("reserves")
        ref.orderByChild("start").equalTo(act).addValueEventListener(
            object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    var reserve = snapshot.getValue(Reserve::class.java)
                    if (reserve != null) {
                        txtActualState2.setTextColor(Color.RED)
                        txtActualState2.text = "Ocupado"
                    } else {
                        txtActualState2.setTextColor(resources.getColor(R.color.colorSecundario))
                        txtActualState2.text = "Disponible"
                    }
                }
                override fun onCancelled(p0: DatabaseError)
                {
                    Log.i(TAG, "Avaliability listener cancelled.")
                }
            }
        )
        calendario = CalenderView(this)
        calendario.setCalenderDayClickListener(
            object: CalenderView.CalenderDayClickListener
            {
                override fun onDayClick(dayStr: String, monthStr: String, yearStr: String)
                {
                    dayPressed(dayStr, monthStr, yearStr)
                }
            })
        reservasFragContainer.addView(calendario)
    }


    // -----------------------------------------------------------------
    // Aux methods.
    // -----------------------------------------------------------------
    private fun updateUI(court: Court)
    {
        Glide.with(this).load(court.imageURL).into(courtImage)
        txtCourtName.text = court.name
    }

    fun attemptReservation(comment: String, date: String, mail: String)
    {
        if(isOnline())
        {
            ref = db.getReference("reserves")
            ref.orderByChild("start").equalTo(date).addListenerForSingleValueEvent(
                object : ValueEventListener
                {
                    override fun onDataChange(ds: DataSnapshot)
                    {
                        val reserves = ds.children
                        var available = true
                        reserves.forEach { temp ->
                            var reservation = temp.getValue(Reserve::class.java)
                            if (reservation != null)
                                if (reservation.courtId.equals(courtId))
                                    available = false
                        }
                        if (available)
                            finishReservation(comment, courtId, date, mail)
                        else
                            showAlertDialog("Realizar reserva", "Este horario ya esta reservado.")
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Log.d(TAG, "Canceled")
                    }
                }
            )
        }
        else
            showAlertDialog(msg = "Error al generar reserva. \nRevise su conexión a internet.")
    }

    fun finishReservation(comment: String, courtId: String, date: String, mail: String)
    {
        var dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.DialogAlert))
        with(dialog)
        {
            setTitle("Confirmar reserva")
            setMessage("Desea confirmar su reserva para el $date")
            setPositiveButton("Aceptar", DialogInterface.OnClickListener{ _, _ ->
                confirmed(comment, courtId, date, mail)
            })
            setNegativeButton("Cancelar", null)
        }
        dialog.create().show()
    }

    private fun confirmed(comment: String, courtId: String, date: String, mail: String)
    {
        val reserve = Reserve(comment, courtId, date, mail, "Reserved")
        ref = db.getReference("reserves")
        val key = ref.push().key
        if (key != null)
            ref.child(key).setValue(reserve)
        // Show reservation confirmation dialog.
        val dView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_class, null)
        val text ="$key"
        val qrEncoder = QRGEncoder(text,null, QRGContents.Type.TEXT, 500)
        try {
            val bitmap = qrEncoder.encodeAsBitmap()
            dView.imgQrReservation.setImageBitmap(bitmap)
        }
        catch(cause: Throwable){Log.i(TAG, "Error generating QR Code.")}
        val parkId = this.getSharedPreferences("PREF_PARK",Context.MODE_PRIVATE).getString("PARK_ID", "")


        val dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.DialogAlert)).setView(dView)
        dialog.setPositiveButton("Aceptar", DialogInterface.OnClickListener{_,_ ->
            val intent = Intent(this, ParqueActividades::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.apply {
                putExtra(EXTRA_MSG, parkId)
            }
            startActivity(intent)
        })
        dialog.show()
    }

    // -----------------------------------------------------------------
    // Fragment management methods.
    // -----------------------------------------------------------------

    private fun addFragmentWithBackStack(fragment: Fragment)
    {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(reservasFragContainer.id, fragment)
        fragmentTransaction.addToBackStack(null) // Con esto el sistema permita ir para atrás.
        fragmentTransaction.commit()
    }

    fun dayPressed(dayStr: String, pMonthStr: String, yearStr: String)
    {
        if(isOnline())
        {
            calendario.visibility = View.INVISIBLE
            txtActualState1.visibility = View.INVISIBLE
            txtActualState2.visibility = View.INVISIBLE
            var monthStr = pMonthStr
            if(monthStr.length == 1)
                monthStr= "0$monthStr"
            txtRealizarReserva.text = "Crear una reserva:   $dayStr/$monthStr/$yearStr"
            var dayFrag = DayFragment.newInstance("$dayStr/$monthStr/$yearStr", "8:00", "19:00")
            addFragmentWithBackStack(dayFrag)
        }
        else
            showAlertDialog()
    }

    fun onReturn()
    {
        calendario.visibility = View.VISIBLE
        txtActualState1.visibility = View.VISIBLE
        txtActualState2.visibility = View.VISIBLE
        txtRealizarReserva.text = "Crear una reserva: "
    }

    // -----------------------------------------------------------------
    // Connectivity methods.
    // -----------------------------------------------------------------

    private fun showAlertDialog(title: String?= "Sin conexión", msg: String?= "No se encuentra conectado a la red. Por favor conectese e intente de nuevo.")
    {
        var dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.DialogAlert))
        with(dialog)
        {
            setTitle(title)
            setMessage(msg)
            setPositiveButton("Aceptar", null)
        }
        dialog.create().show()
    }

    private fun isOnline(): Boolean
    {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}
