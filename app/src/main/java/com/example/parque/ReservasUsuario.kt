package com.example.parque

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.iterator
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.parque.models.Court
import com.example.parque.models.Reserve
import com.example.parque.room.entity.RoomCourt
import com.example.parque.room.viewModel.CourtViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_reservas_usuario.*
import kotlinx.coroutines.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

private const val TAG = "MisReservas"
class ReservasUsuario : AppCompatActivity() {

    private lateinit var auth:FirebaseAuth
    private lateinit var db:FirebaseDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservas_usuario)
        supportActionBar?.hide()
        auth = FirebaseAuth.getInstance()
        db = Firebase.database
        val txtCorreoUsuario = findViewById<TextView>(R.id.txtEmailUsuario)
        val sharedPreferences = getSharedPreferences(TPS, Context.MODE_PRIVATE)
        val correo = sharedPreferences.getString("correo","")
        txtCorreoUsuario.text = correo
        try {
            val referenciaReservas = db.getReference("reserves").orderByChild("start")
            val referenciaCourts = db.getReference("courts")
            val lly = findViewById<LinearLayout>(R.id.linearLayout_misReservas)
            referenciaReservas.addValueEventListener(
                object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("Not yet implemented")
                    }


                    override fun onDataChange(ds: DataSnapshot) {
                        lly.removeAllViews()
                        for (snapshot in ds.children) {
                            var reserva: Reserve? = snapshot.getValue(Reserve::class.java)
                            if(reserva!!.user == correo) {
                                val start = reserva!!.start
                                val courtId = reserva!!.courtId
                                val btn = Button(applicationContext)
                                btn.tag = snapshot.key
                                val court = referenciaCourts.child(courtId!!)
                                var text = ""
                                court.addListenerForSingleValueEvent(
                                    object : ValueEventListener {
                                        override fun onDataChange(p0: DataSnapshot) {
                                            val court: Court? = p0.getValue(Court::class.java)
                                            val courtName = court!!.name
                                            text = "Court Name: $courtName \n Start: $start"
                                            btn.setText(text)
                                            btn.setOnClickListener {
                                                abrirReserva(snapshot.key!!)
                                            }
                                            val formatter = DateTimeFormatter.ofPattern("d/MM/yyyy H:mm:ss")
                                            val currentDateTime = LocalDateTime.now()
                                            val reserveDateTime:LocalDateTime = LocalDateTime
                                                .parse(start, formatter) as LocalDateTime
                                            if(currentDateTime > reserveDateTime){
                                                btn.isEnabled = false
                                            }

                                            lly.addView(btn)
                                        }

                                        override fun onCancelled(p0: DatabaseError) {
                                            TODO("Not yet implemented")
                                        }
                                    }
                                )
                            }
                        }
                    }

                }
            )
        }catch(cause:Throwable){
            Log.e(TAG,"No fue posible obtener las reservas del usuario")
        }
    }

    private fun limpiarBotones(){
        val referenciaReserves = db.getReference("reserves")
        val layout = findViewById<LinearLayout>(R.id.linearLayout_misReservas)
        if(layout != null){
            for(view in layout){
                val btn:Button = view as Button
                val id = btn.tag as String
                val courtActual = referenciaReserves.child(id)
                courtActual.addListenerForSingleValueEvent(
                    object:ValueEventListener{
                        override fun onCancelled(p0: DatabaseError) {
                            TODO("Not yet implemented")
                        }
                        override fun onDataChange(ds: DataSnapshot) {
                            val reserve = ds.getValue(Reserve::class.java)
                            if(reserve == null){
                                layout.removeView(btn)
                                Log.w(TAG,"Se removió el botón de la reserve $id porque ya era obsoleto")
                            }
                        }
                    }
                )
            }
        }
    }


    fun abrirReserva(reserveId:String){
        val intent = Intent(this, ReserveQR::class.java)
        intent.apply {
            putExtra(EXTRA_MSG, reserveId)
        }
        startActivity(intent)
    }

    fun cerrarSesion(view: View){
        val intent = Intent(this, InicioSesion::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        auth.signOut()
        startActivity(intent)
    }

}
