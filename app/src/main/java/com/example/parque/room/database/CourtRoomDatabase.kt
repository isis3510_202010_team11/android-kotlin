package com.example.parque.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.parque.room.dao.CourtDao
import com.example.parque.room.entity.RoomCourt
import kotlinx.coroutines.CoroutineScope

@Database(entities = arrayOf(RoomCourt::class), version = 1)
abstract class CourtRoomDatabase:RoomDatabase() {

    abstract fun courtDao(): CourtDao

    companion object{

        @Volatile
        private var INSTANCE: CourtRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): CourtRoomDatabase {
            val tempInstance =
                INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CourtRoomDatabase::class.java,
                    "court_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}