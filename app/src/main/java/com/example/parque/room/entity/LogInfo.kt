package com.example.parque.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "log_Info")
data class LogInfo(@PrimaryKey @ColumnInfo(name="correo") val correo:String) {
}