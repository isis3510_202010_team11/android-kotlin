package com.example.parque.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.parque.room.entity.LogInfo

@Dao
interface LogInfoDao {
    @Query("SELECT * from log_Info")
    fun getAll():LiveData<List<LogInfo>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(logInfo: LogInfo)

    @Update
    suspend fun update(logInfo: LogInfo)

    @Query("DELETE FROM log_Info")
    suspend fun deleteAll()
}