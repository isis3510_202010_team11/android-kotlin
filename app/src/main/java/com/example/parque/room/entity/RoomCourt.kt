package com.example.parque.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "court")
data class RoomCourt(
    @PrimaryKey
    @ColumnInfo(name="courtId")
    val id:String,

    @ColumnInfo(name="imageURL")
    val imageURL:String,

    @ColumnInfo(name="name")
    val name:String,

    @ColumnInfo(name="parkId")
    val parkId:String,

    @ColumnInfo(name="type")
    val type:String
)