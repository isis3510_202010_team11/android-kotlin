package com.example.parque.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.parque.room.dao.ParkDao
import com.example.parque.room.entity.RoomPark
import kotlinx.coroutines.CoroutineScope

@Database(entities = arrayOf(RoomPark::class), version = 1)
abstract class ParkRoomDatabase:RoomDatabase() {

    abstract fun parkDao(): ParkDao

    companion object{

        @Volatile
        private var INSTANCE: ParkRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope:CoroutineScope
            ): ParkRoomDatabase {
            val tempInstance =
                INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ParkRoomDatabase::class.java,
                    "park_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}