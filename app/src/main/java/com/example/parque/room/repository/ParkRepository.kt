package com.example.parque.room.repository

import androidx.lifecycle.LiveData
import com.example.parque.room.dao.ParkDao
import com.example.parque.room.entity.RoomPark


class ParkRepository(private val parkDao: ParkDao) {

    val allParks:LiveData<List<RoomPark>> = parkDao.getAll()

    suspend fun getPark(id:String): RoomPark {
        return parkDao.getPark(id)
    }

    suspend fun insert(park: RoomPark){
        parkDao.insert(park)
    }

    suspend fun update(park: RoomPark){
        parkDao.update(park)
    }

    suspend fun delete(){
        parkDao.deleteAll()
    }

}