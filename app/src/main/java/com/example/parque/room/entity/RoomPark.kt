package com.example.parque.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "park")
data class RoomPark(
    @PrimaryKey
    @ColumnInfo(name="parkId")
    val id:String,

    @ColumnInfo(name="address")
    val address:String,

    @ColumnInfo(name="city")
    val city:String,

    @ColumnInfo(name="imageURL")
    val imageURL:String,

    @ColumnInfo(name="latitude")
    val latitude:Double,

    @ColumnInfo(name="longitude")
    val longitude:Double,

    @ColumnInfo(name="name")
    val name:String
)