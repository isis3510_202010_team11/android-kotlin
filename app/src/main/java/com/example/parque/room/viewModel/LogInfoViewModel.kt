package com.example.parque.room.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.parque.room.database.LogInfoRoomDatabase
import com.example.parque.room.entity.LogInfo
import com.example.parque.room.repository.LogInfoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LogInfoViewModel(application:Application):AndroidViewModel(application) {

    private val repository: LogInfoRepository

    val allLogInfo:LiveData<List<LogInfo>>

    init{
        val logInfoDao = LogInfoRoomDatabase.getDatabase(application, viewModelScope).logInfoDao()
        repository =
            LogInfoRepository(logInfoDao)
        allLogInfo = repository.allLogInfo
    }

    fun insert(logInfo: LogInfo) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(logInfo)
    }

    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.delete()
    }
}