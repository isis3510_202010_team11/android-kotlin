package com.example.parque.room.repository

import androidx.lifecycle.LiveData
import com.example.parque.room.dao.LogInfoDao
import com.example.parque.room.entity.LogInfo

class LogInfoRepository(private val logInfoDao: LogInfoDao) {

    val allLogInfo: LiveData<List<LogInfo>> = logInfoDao.getAll()

    suspend fun insert(logInfo: LogInfo){
        logInfoDao.insert(logInfo)
    }


    suspend fun delete(){
        logInfoDao.deleteAll()
    }
}