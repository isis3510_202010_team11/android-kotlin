package com.example.parque.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.parque.room.entity.RoomCourt

@Dao
interface CourtDao {

    @Query("SELECT * FROM court")
    fun getAll():LiveData<List<RoomCourt>>

    @Query("SELECT * FROM court WHERE courtId LIKE :courtId")
    fun getCourt(courtId:String):RoomCourt

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(court:RoomCourt)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(court:RoomCourt)

    @Query("DELETE FROM court")
    suspend fun deleteAll()
}