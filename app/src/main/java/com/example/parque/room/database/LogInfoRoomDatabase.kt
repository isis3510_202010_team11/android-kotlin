package com.example.parque.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.parque.room.dao.LogInfoDao
import com.example.parque.room.entity.LogInfo
import kotlinx.coroutines.CoroutineScope

@Database(entities = arrayOf(LogInfo::class), version = 1)
abstract class LogInfoRoomDatabase: RoomDatabase() {
    abstract fun logInfoDao(): LogInfoDao

    companion object{
        @Volatile
        private var INSTANCE: LogInfoRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope:CoroutineScope
        ): LogInfoRoomDatabase {
            val tempInstance =
                INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    LogInfoRoomDatabase::class.java,
                    "log_info_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}