package com.example.parque.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.parque.room.entity.RoomPark


@Dao
interface ParkDao {

    @Query("SELECT * FROM park")
    fun getAll(): LiveData<List<RoomPark>>

    @Query("SELECT * FROM park WHERE parkId LIKE :id")
    fun getPark(id:String): RoomPark

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(park: RoomPark)

    @Update
    suspend fun update(park: RoomPark)

    @Query("DELETE FROM park")
    suspend fun deleteAll()
}