package com.example.parque.room.repository

import androidx.lifecycle.LiveData
import com.example.parque.room.dao.CourtDao
import com.example.parque.room.entity.RoomCourt

class CourtRepository(private val courtDao: CourtDao) {

    val allCourts: LiveData<List<RoomCourt>> = courtDao.getAll()

    suspend fun getCourt(courtId:String):RoomCourt{
        return courtDao.getCourt(courtId)
    }

    suspend fun insert(court:RoomCourt){
        courtDao.insert(court)
    }

    suspend fun update(court:RoomCourt){
        courtDao.update(court)
    }

    suspend fun deleteAll(){
        courtDao.deleteAll()
    }
}