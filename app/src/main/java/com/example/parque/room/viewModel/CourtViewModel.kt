package com.example.parque.room.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.parque.room.database.CourtRoomDatabase
import com.example.parque.room.entity.RoomCourt
import com.example.parque.room.repository.CourtRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CourtViewModel(application:Application):AndroidViewModel(application){

    private val repository:CourtRepository

    val allCourts:LiveData<List<RoomCourt>>

    init{
        val courtDao = CourtRoomDatabase.getDatabase(application, viewModelScope).courtDao()
        repository = CourtRepository(courtDao)
        allCourts = repository.allCourts
    }

    fun getCourt(courtId:String) = viewModelScope.launch(Dispatchers.IO) {
        repository.getCourt(courtId)
    }

    fun insert(court:RoomCourt) = viewModelScope.launch(Dispatchers.IO){
        repository.insert(court)
    }

    fun update(court:RoomCourt) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(court)
    }

    fun deleteAll() = viewModelScope.launch(Dispatchers.IO){
        repository.deleteAll()
    }
}