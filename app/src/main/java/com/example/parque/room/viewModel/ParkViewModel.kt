package com.example.parque.room.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.parque.room.database.ParkRoomDatabase
import com.example.parque.room.entity.RoomPark
import com.example.parque.room.repository.ParkRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ParkViewModel(application: Application):AndroidViewModel(application) {

    private val repository: ParkRepository

    val allParks:LiveData<List<RoomPark>>

    init{
        val parkDao = ParkRoomDatabase.getDatabase(application, viewModelScope).parkDao()
        repository = ParkRepository(parkDao)
        allParks = repository.allParks
    }

    fun getPark(id:String) = viewModelScope.launch(Dispatchers.IO) {
        repository.getPark(id)
    }

    fun insert(park: RoomPark) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(park)
    }

    fun update(park: RoomPark) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(park)
    }

    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.delete()
    }
}